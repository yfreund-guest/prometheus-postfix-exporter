Source: prometheus-postfix-exporter
Section: net
Priority: optional
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Daniel Swarbrick <daniel.swarbrick@cloud.ionos.com>
Build-Depends: debhelper (>= 11),
               dh-golang,
               golang-github-coreos-go-systemd-dev,
               golang-github-prometheus-client-golang-dev,
               golang-github-influxdata-tail-dev,
               golang-gopkg-alecthomas-kingpin.v3-dev,
               golang-go
Standards-Version: 4.3.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/prometheus-postfix-exporter
Vcs-Git: https://salsa.debian.org/go-team/packages/prometheus-postfix-exporter.git
Homepage: https://github.com/kumina/postfix_exporter
XS-Go-Import-Path: github.com/kumina/postfix_exporter

Package: prometheus-postfix-exporter
Architecture: any
Depends: postfix, ${misc:Depends}, ${shlibs:Depends}
Built-Using: ${misc:Built-Using}
Description: Prometheus exporter for Postfix mail servers
 Prometheus exporter which scrapes the systemd journal of Postfix MTA daemons
 to expose various metrics such as SMTP connection counts, message delivery
 statuses / delays etc. This exporter has a slight advantage over the mtail
 log parser approach, since it also connects to the Postfix admin Unix socket
 to expose metrics about messages currently in the queue.
